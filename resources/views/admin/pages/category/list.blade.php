@extends('admin.master')
@section('title',"Quản lí danh mục")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí danh mục</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách danh mục</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                  <div class="col-md-8 col-md-offset-2">
                      <div class="box">
                          <div class="box-header with-border">
                              <h3 class="box-title">Danh sách danh mục</h3>
                          </div>
                          <button type="button" id="modal_add" class="btn btn-info btn-success pull-right" data-toggle="modal" style="margin: 5px 0px">Thêm danh mục</button>
                          <!-- /.box-header -->

                        <div class="content-table">
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th>Stt</th>
                                        <th>Tên</th>
                                        <th>Trạng thái</th>
                                        <th>Sửa</th>
                                        <th>Xóa</th>
                                    </tr>
                                    <?php $stt=1?>
                                    @foreach($cate as $item)
                                        <tr>
                                            <th>{{$stt++}}</th>
                                            <th>{{$item->name}}</th>
                                            <th>@if($item->status==2){{"Hiện"}}
                                                @else($item->status==1){{"Ẩn"}}
                                                @endif
                                            </th>
                                            <th><button type="button" class="button_update" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
                                            <th><button type="button" class="button_del" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                {{$cate->links()}}
                            </div>
                        </div>

                  </div>
              </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm danh mục</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger print-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <form action=""  method="post" id="formaddcate" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <label for="">Danh mục cha</label>
                                            <select name="sltname"   class="form-control">
                                                <option value="">--chọn--</option>
                                                <option value="0">root</option>
                                                      {{Menu_mutil($data)}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tên danh mục</label>
                                        <input type="text" class="form-control" name="name" placeholder="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Trạng thái</label>
                                        <select name="status" class="form-control">
                                            <option value="">--chọn--</option>
                                            <option value="1">Ẩn</option>
                                            <option value="2">Hiện</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Icon</label>
                                        <input type="file" name="txticon">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary add_cate" >Thêm</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
 {{--modal sủa--}}
            <div class="modal fade" id="myModalupdate" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa danh mục</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger print-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <form action=""  method="post" id="formeditcate" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">

                                        <div class="form-group">
                                            <label for="">Danh mục cha</label>
                                            <select name="sltname"   class="form-control" id="slt_edit">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Tên danh mục</label>
                                        <input type="text" class="form-control" name="name" placeholder="name" id="namecate">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Trạng thái</label>
                                        <select name="status" class="form-control" id="sltstatus">
                                            <option value="">--chọn--</option>
                                            <option value="1">Ẩn</option>
                                            <option value="2">Hiện</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Icon</label>
                                        <input type="file" name="txticon">
                                    </div>
                                    <div class="text"></div>
                                </div>
                                <!-- /.box-body -->
                                <input type="hidden" name="idsp" id="idsp" class="idsp">
                                <input type="hidden" name="idpaginate" id="idpaginate" class="idpaginate">
                                <button type="submit" class="btn btn-primary edit_cate" >Sửa</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection()