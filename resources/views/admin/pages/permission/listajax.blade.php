<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Stt</th>
        <th>Name</th>
        <th>Permission_for</th>
        <th>Edit</th>
        <th>Del</th>
    </tr>
    <?php $stt=1?>
    @foreach($per as $item)
        <tr>
            <th>{{$stt++}}</th>
            <th>{{$item->name}}</th>
            <th>{{$item->permission_for}}</th>
            <th><button class="btn btn-warning btn-edit-per" value="{{ $item->id }}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></th>
            <th><button class="btn btn-danger btn-del-per" value="{{ $item->id }}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></th>
        </tr>
    @endforeach
    </tbody>
</table>