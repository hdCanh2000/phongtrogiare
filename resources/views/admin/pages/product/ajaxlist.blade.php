<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Stt</th>
        <th>Name</th>
        <th>Phone</th>
        <th>end</th>
        <th>pay</th>
        <th>status</th>
        <th>Edit</th>
        <th>Del</th>
    </tr>
    <?php $stt=1?>
    @foreach($pro as $item)
        <tr>
            <td>{{$stt++}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->phone}}</td>
            <td>{{$item->end_day}}</td>
            <td>@if($item->pending==1)
                    Đã thanh toán
                @else
                    Chưa thanh toán
                @endif</td>
            <td>@if($item->status==1){{"Hiện"}}
                @else($item->status==2){{"Ẩn"}}
                @endif</td>
            <td><button type="button" class="btn btn-danger button_update_product" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></td>
            <td><button type="button" class="btn btn-warning button_del_product" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></td>
        </tr>
    @endforeach
    </tbody>
</table>