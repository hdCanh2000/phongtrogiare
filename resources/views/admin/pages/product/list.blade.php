@extends('admin.master')
@section('title',"Quản lí dự án")
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Quản lí dự án</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
                <li class="active">Danh sách dự án</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danh sách dự án</h3>
                        </div>
                        <button type="button" class="btn btn-info btn-success pull-right" data-toggle="modal" id="modaladdpro" style="margin: 5px 0px">Thêm dự án</button>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="content-table">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Stt</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>end</th>
                                    <th>pay</th>
                                    <th>status</th>
                                    <th>Edit</th>
                                    <th>Del</th>
                                </tr>
                                <?php $stt=1?>
                               @foreach($pro as $item)
                                   <tr>
                                       <td>{{$stt++}}</td>
                                       <td>{{$item->name}}</td>
                                       <td>{{$item->phone}}</td>
                                       <td>{{$item->end_day}}</td>
                                       <td>@if($item->pending==1)
                                               Đã thanh toán
                                               @else
                                               Chưa thanh toán
                                       @endif</td>
                                       <td>@if($item->status==1){{"Hiện"}}
                                           @else($item->status==2){{"Ẩn"}}
                                           @endif</td>
                                       <td><button type="button" class="btn btn-danger button_update_product" value="{{$item->id}}"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></button></td>
                                       <td><button type="button" class="btn btn-warning button_del_product" value="{{$item->id}}"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button></td>
                                   </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="#">«</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>

            {{--modal them--}}

            <div class="modal fade" id="modaladdproduct" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thêm dự án</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="addpro" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="box-body">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">Tên dự án</label>
                                           <input type="text" class="form-control"   placeholder="Tên dự án" name="name">
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Danh mục</label>
                                            <select name="sltcate" class="form-control">
                                                <option value="">--chọn--</option>
                                                @foreach($cate as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Diện tích</label>
                                            <input type="text" class="form-control" name="dientich" placeholder="Diện tích">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Giá</label>
                                            <input type="text" class="form-control" name="gia" placeholder="Giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Số điện thoại</label>
                                            <input type="text" class="form-control" name="sdt" placeholder="sdt">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Họ tên chủ nhà</label>
                                            <input type="text" class="form-control" name="name_own" placeholder="tên">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Bắt đầu</label>
                                            <input type="text" class="form-control" name="starttime" id="datestart" placeholder="Giá">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Kết thúc</label>
                                            <input type="text" class="form-control" name="endtime" id="dateend" placeholder="Giá">
                                        </div>
                                    </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">Trạng thái</label>
                                           <select name="status"   class="form-control">
                                               <option value="" >--chọn--</option>
                                               <option value="1">Hoạt động</option>
                                               <option value="2">Không hoạt động</option>
                                           </select>
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Khu vực</label>
                                            <select name="diadiem"   class="form-control">
                                                <option value="" >--chọn--</option>
                                                @foreach($address as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Địa chỉ</label>
                                            <input type="text" class="form-control" name="address_detail">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                              <label >Thành tiền(Đv:vnđ)</label>
                                              <input type="text" class="form-control" id="thanhtien" name="thanhtien">
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Pay</label>
                                            <select name="pending"   class="form-control">
                                                <option value="" >--chọn--</option>
                                                <option value="1">Đã thanh toán</option>
                                                <option value="2">Chưa thanh toán</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label >Gmap</label>
                                            <input type="text" class="form-control" id="gmap" name="gmap" value="">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label >Icon</label>
                                        <input type="file" class="form-control" name="imageicon" id="imageicon">
                                    </div>
                                    <div class="preview-icon">

                                    </div>
                                    <div class="form-group">
                                        <label >Image</label>
                                        <input type="file" class="form-control" name="imageproduct[]" id="imageproduct" multiple>
                                    </div>
                                    <div class="preview-image">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Mô tả</label>
                                        <textarea  class="form-control" name="mota" id="" cols="30" rows="10"></textarea>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>


            {{--{{modal sửa}}--}}
            <div class="modal fade" id="modal_update_pro" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Sửa dự án</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" action="" method="post" id="editproject">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label >Gmap</label>
                                        <input type="text" class="form-control" name="gmap" id="gmap">
                                        <input type="hidden" class="form-control" name="id" id="idpro_">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Trạng thái</label>
                                        <select name="status"   class="form-control" id="status">

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Pay</label>
                                        <select name="pay"   class="form-control" id="pay">
                                        </select>
                                    </div>

                                    </div>

                                <!-- /.box-body -->
                                <button type="submit" class="btn btn-primary btneditpro">Sửa</button>
                            </form>
                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

@endsection()
