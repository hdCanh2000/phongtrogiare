<?php

namespace App\Http\Controllers;
use App\Admin;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;

class AdminController extends Controller
{
    public function index(){
        if(Auth::guard('admin')->check()){
            return view('admin.pages.dasboard');
        }
        else{
            return view('login.login');
        }
    }

  public function getLogin(){
      if(Auth::guard('admin')->check()){
          return redirect()->route('getDasboard');
      }
      else{
          return view('login.login');
      }
//      return view('login.login');
  }

  public function postLogin(Request $request){
        $name=$request->name;
        $password=$request->password;

      if ( Auth::guard('admin')->attempt(['name' => $name, 'password' =>$password,'role'=>1,'status'=>1])) {

          return redirect()->route('getDasboard');

      }
      else{
          echo"loi";
      }
  }

  public function getLogout(){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

  public function getListView(){
      $user=Admin::orderBy('id','desc')->paginate(5);
      return view('admin.pages.user.list',compact('user'));
  }

  public function getAdmin(){
        $user=Admin::orderBy('id','desc')->paginate(5);
       return view('admin.pages.user.listajax',compact('user'));
  }

  public function addAddAdmin(AdminRequest $request){
      try{
          $user=new Admin();
          $user->name=$request->name;
          $user->email=$request->email;
          $user->password=Hash::make($request->password);
          $user->role=$request->role;
          $user->status=$request->status;
          $user->save();
         return $this->getAdmin();
      }
      catch (\Exception $e){
          return $e->getMessage();
      }
  }

  public function getEditAdmin(Request $req){
        try{
            $admin=Admin::findOrFail($req->id);
            return response()->json($admin);
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
  }

  public function postEditAdmin(AdminRequest $request){
        try{
            $user=Admin::findOrFail($request->id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->role=$request->role;
            $user->status=$request->status;
            $user->update();
            return $this->getAdmin();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
  }

  public function getDelAdmin(Request $request){
        try{
            $user=Admin::findOrFail($request->id);
            $user->delete();
            return $this->getAdmin();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
  }


}
