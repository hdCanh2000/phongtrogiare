<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionRequest;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(){
        $per=Permission::paginate(10);
        return view('admin.pages.permission.list',compact('per'));
    }
    public function getPer(){
        $per=Permission::paginate(10);
        return view('admin.pages.permission.listajax',compact('per'));
    }

    public function store(PermissionRequest $request){
        try{
            $per=new Permission();
            $per->name=$request->name;
            $per->permission_for=$request->name_for;
            $per->save();
            return $this->getPer();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function edit(Request $request){
        try{
            $per=Permission::find($request->id);
            return response()->json($per);
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function update(PermissionRequest $request){
        try{
            $per=Permission::find($request->id);
            $per->name=$request->name;
            $per->permission_for=$request->name_for;
            $per->update();
            return $this->getPer();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
    public function delete(Request $request){
        try{
            $per=Permission::find($request->id);
            $per->delete();
            return $this->getPer();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
