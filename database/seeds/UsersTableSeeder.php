<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "manh",
            'email' => 'truongmanh93cntt@gmail.com',
            'password' => bcrypt('manh123'),
            'role'=>'1',
            'status'=>'1',
        ]);
    }
}
