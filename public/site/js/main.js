(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

},{}]},{},[1])

$(document).on('click','.product_item',function () {
    var id=$(this).attr('pro_id');
    $.ajax({
        url:"/detail",
        method:"GET",
        data:{id:id},
        dataType:"html",
    })
        .done(function(data){
            $(".products").hide();
            $(".customerpost").hide();
            $(".content").show();
            $(".content").html(data);

        })
        .fail(function(erro){
            console.log(erro.statusText);
        });
});


$(document).on("click",".btn-xoa",function(e){
    e.preventDefault();
    $(".content-detail").hide();
    $(document).find('input').removeAttr("checked");
    $(".products").show();
});


$(document).on("click",".cate_product",function(){
    var id=$(this).attr('cate_id');
    // var datakv=$("#formkv").serialize();

    $('.label_cate').html($(this).attr('cate_name'));
    $('.cate_id').val(id);

    $.ajax({
        url:"/datatype",
        method:"GET",
        data:{id:id},
        dataType:"html",
    })
        .done(function(data){
            $(".products").hide();
            $(".customerpost").hide();
            $(".content").show();
            $(".content").html(data);

        })
        .fail(function(erro){
            console.log(erro.statusText);
        });
});

$(document).on('change','#formkv',function (e) {
    e.preventDefault();
    var data=$(this).serialize();

    $.ajax({
        url:"/data_type",
        method:"GET",
        data:data,
        dataType:"html",
    })
        .done(function(data){
            // console.log(data);
            $(".products").hide();
            $(".customerpost").hide();
            $(".content").show();
            $(".content").html(data);

        })
        .fail(function(erro){
            console.log(erro.statusText);
        });

});

$(document).on('click','.dangbai',function (e) {
    e.preventDefault();
    $.ajax({
        url:"/dang-bai",
        method:"GET",
        dataType:"html",
    })
        .done(function(data){

            $(".customerpost").html(data);

        })
        .fail(function(erro){
            console.log(erro.statusText);
        }).always(function () {
        var jump = $('.dangbai').attr('href');
        $(".products").hide();
        $(".content").hide();
        $(".content-detail").hide();
        $(".customerpost").show();
        $(".thanhtoan").hide();
        $(".thanks").hide();
        var new_position = $('#'+jump).offset();
        window.scrollTo( 389.5,1323.421875);
        return false;
    });
});

$(document).on('change','#imageicon1',function (e) {
    var file=e.target.files[0];
    var reader=new FileReader();
    if (file && file.type.match('image.*')) {
        reader.readAsDataURL(file);
    } else {
        $(".preview-icon1").html("Image không được chọn hoặc image không đúng định dạng");
    }
    reader.onload=function (ev) {
        var temp='<img src="'+ev.target.result+'" alt="" width="80px">';
        $(".preview-icon1").html(temp);
    }
});

$(document).on('change','#dateend1',function (e) {
    e.preventDefault();
    var datestart=new Date($("#datestart1").val())
    var dateend=new Date($(this).val());
    if(datestart.getMonth()<dateend.getMonth()){
      var lastDaystart = new Date(datestart.getFullYear(),datestart.getMonth() + 1, 0);
       var beginDayend=new Date(dateend.getFullYear(), dateend.getMonth(), 1);
       if(dateend.getDate()==beginDayend.getDate()){
           soday=(lastDaystart.getDate()-datestart.getDate())+1;
           thanhtien=soday*10000;
           $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
           $("#thanhtien1").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
       }
       else {
           soday=(lastDaystart.getDate()-datestart.getDate())+(dateend.getDate()-beginDayend.getDate());
           thanhtien=soday*10000;
           $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
           $("#thanhtien1").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
       }

    }
    else if(datestart.getMonth()==dateend.getMonth()){
        soday=dateend.getDate()-datestart.getDate();
        thanhtien=soday*10000;
        $("#thanhtien").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $("#thanhtien1").val(thanhtien.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    }
    else{
        alert("bạn bịp tôi sao");

        return false;
    }

});
// $(document).on('click','.cus_upload',function (e) {
//         e.preventDefault();
//     var form_data = new FormData($("#cuspost")[0]);
//     $.ajax({
//         url: '/dang-bai-cus',
//         type: 'post',
//         data: form_data,
//         dataType: 'json',
//         // async: false,
//         processData: false,
//         contentType: false,
//     }).done(function (data) {
//         console.log(data.message);
//         if(data.message){
//             $(".thanhtoan").show();
//             $("#dang-bai").hide();
//         }
//     }).fail(function (data) {
//         console.log(data);
//         // var response = JSON.parse(data.responseText);
//         // printErrorMsg(response.errors);
//     });
// });

$(document).on('click','.btn-success',function (e) {
   e.preventDefault();
    $(".thanhtoan").hide();
    $(".thanks").show();
});

//add project


// $("#cuspostform").validate({
// //specify the validation rules
//     rules: {
//         name: "required",
//         // starttime:"required",
//         // endtime:"required",
//         // mota: "required",
//         // sltcate: "required",
//         // diadiem: "required",
//         // address_detail: "required",
//         // name_own:"required",
//         // sdt: {
//         //     required:true,
//         //     number:true
//         // },
//         // // thoigian:"required",
//         // gia:{
//         //     required:true,
//         //     number:true
//         // },
//         // dientich: {
//         //     required:true,
//         //     number:true
//         // },
//         // imageproduct: {
//         //     required: true,
//         // },
//         // imageicon:{
//         //     required: true,
//         // },
//     },
// //specify validation error messages
//     messages: {
//         name: "Tên không được để trống",
//         // starttime: "Thời gian bắt đầu không được để trống",
//         // starttime: "Thời gian kết thúc không được để trống",
//         // diadiem: "Khu vực không được để trống",
//         // address_detail: "Địa chỉ không được để trống",
//         // name_own: "Tên chủ nhà không được để trống",
//         // mota: "Mô tả không được để trống",
//         // sltcate:"Danh mục cha không được để trống",
//         //
//         // sdt:{
//         //     required:"Số điện thoại không được để trống",
//         //     number:"Số điện thoại phải là số"
//         // },
//         // gia:{
//         //     required:"giá không được để trống",
//         //     number:"giá phải là số"
//         // },
//         // dientich:{
//         //     required:"Diện tích không được để trống",
//         //     number:"Diện tích phải là số"
//         // },
//         // imageproduct: {
//         //     required: "Ảnh không được để trống",
//         //
//         // },
//         // imageicon:{
//         //     required: "icon không được để trống",
//         //
//         // },
//     },
//     submitHandler: function(form){
//         $(form).submit();
//     }
// });

