-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.30-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table shophot.address: ~8 rows (approximately)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'DH CNTT', 2, '2018-03-23 03:21:20', '2018-03-23 03:21:20'),
	(2, 'ĐH Công Nghiệp', 2, '2018-03-23 03:24:58', '2018-03-23 03:24:58'),
	(3, 'DH Nông Lâm', 2, '2018-03-23 03:26:09', '2018-03-23 03:26:09'),
	(4, 'DH Khoa Hoc', 2, '2018-03-23 03:27:48', '2018-03-23 03:27:48'),
	(5, 'DH Kinh Tế', 2, '2018-03-23 03:29:08', '2018-03-23 03:29:08'),
	(6, 'ĐH Y', 2, '2018-03-23 03:31:27', '2018-03-23 03:31:27'),
	(7, 'CĐ Y', 2, '2018-03-23 03:34:23', '2018-03-23 03:34:23'),
	(8, 'CĐ Kinh Tế', 2, '2018-03-23 03:36:21', '2018-03-23 03:36:21');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;

-- Dumping data for table shophot.admin: ~2 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `name`, `email`, `password`, `role`, `status`, `images`, `created_at`, `updated_at`, `remember_token`, `password_reset`) VALUES
	(4, 'tuan', 'truongtuan96@gmail.com', '$2y$10$uMVeDqbUPf8h0zvn0.31MecBrn4hPjW4iPKPa3bmz/lkB7U2Qa.PO', 1, 1, NULL, '2018-03-21 14:13:29', '2018-03-21 14:13:29', NULL, NULL),
	(6, 'manh', 'truongmanh93cntt@gmail.com', '$2y$10$S5J54T4ckx52j1yZj4L9WOPQGzgm6SJB9MBtUWKBIdBNktWRVi3VS', 1, 1, NULL, '2018-03-21 14:20:14', '2018-03-21 14:20:14', '8rHWtUK0fnFqHhCZvQ4goPm7VTllGTf6NmXm4sTKfYYzmIN20xKW8Lh26KJc', NULL);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping data for table shophot.categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`, `alias`, `parent_id`, `status`, `icon`, `created_at`, `updated_at`, `created_by`) VALUES
	(1, 'Bán', 'ban.html', 0, 2, NULL, '2018-03-07 17:02:42', '2018-03-07 17:02:42', 1),
	(2, 'Cho thuê', 'cho-thue.html', 0, 2, NULL, '2018-03-20 16:20:55', '2018-03-20 16:20:55', 1);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table shophot.image_product: ~13 rows (approximately)
/*!40000 ALTER TABLE `image_product` DISABLE KEYS */;
INSERT INTO `image_product` (`id`, `image`, `product_id`, `created_at`, `updated_at`) VALUES
	(1, '1521859812.1.jpg', 1, '2018-03-24 02:50:12', '2018-03-24 02:50:12'),
	(2, '1521859953.2.jpg', 2, '2018-03-24 02:52:33', '2018-03-24 02:52:33'),
	(3, '1521859953.3.jpg', 2, '2018-03-24 02:52:33', '2018-03-24 02:52:33'),
	(4, '1521860251.3.jpg', 3, '2018-03-24 02:57:31', '2018-03-24 02:57:31'),
	(5, '1521860401.1.jpg', 4, '2018-03-24 03:00:01', '2018-03-24 03:00:01'),
	(6, '1521860768.5.JPG', 5, '2018-03-24 03:06:08', '2018-03-24 03:06:08'),
	(7, '1521860893.6.jpg', 6, '2018-03-24 03:08:13', '2018-03-24 03:08:13'),
	(8, '1521860983.7.jpg', 7, '2018-03-24 03:09:43', '2018-03-24 03:09:43'),
	(9, '1521861088.8.jpg', 8, '2018-03-24 03:11:28', '2018-03-24 03:11:28'),
	(10, '1521861294.6.jpg', 9, '2018-03-24 03:14:54', '2018-03-24 03:14:54'),
	(11, '1522118998.18a-pham-van-dong-phuong-hiep-binh-chanh-quan-thu-duc-thanh-pho-ho-chi-minh-000008297001.jpg', 10, '2018-03-27 02:49:58', '2018-03-27 02:49:58'),
	(12, '1522118998.file.358455.jpg', 10, '2018-03-27 02:49:58', '2018-03-27 02:49:58'),
	(13, '1522120909.g1.PNG', 11, '2018-03-27 03:21:49', '2018-03-27 03:21:49'),
	(14, '1522133664.20160628_125941_e34e31e508722eecf55c70a837a93bec.jpg', 12, '2018-03-27 06:54:24', '2018-03-27 06:54:24'),
	(15, '1522133664.khong-gian-ben-trong-mau-nha-dep-co-5-phong-tro-o-di-an-binh-duong.jpg', 12, '2018-03-27 06:54:24', '2018-03-27 06:54:24');
/*!40000 ALTER TABLE `image_product` ENABLE KEYS */;

-- Dumping data for table shophot.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping data for table shophot.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping data for table shophot.permissions: ~11 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `permission_for`, `created_at`, `updated_at`) VALUES
	(2, 'edit_cate', 'cate', '2018-03-19 14:18:34', '2018-03-19 14:18:34'),
	(4, 'view_cate', 'cate', '2018-03-19 14:19:10', '2018-03-19 14:19:10'),
	(7, 'del_product', 'product', '2018-03-19 14:20:59', '2018-03-19 14:20:59'),
	(8, 'view_product', 'product', '2018-03-19 14:21:11', '2018-03-19 14:21:11'),
	(9, 'add_user', 'user', '2018-03-19 14:21:30', '2018-03-19 14:21:30'),
	(10, 'edit_user', 'user', '2018-03-19 14:21:40', '2018-03-19 14:21:40'),
	(11, 'del_user', 'user', '2018-03-19 14:21:48', '2018-03-19 14:21:48'),
	(12, 'view_user', 'user', '2018-03-19 14:21:58', '2018-03-19 14:21:58'),
	(13, 'add_cate', 'cate', '2018-03-20 16:16:53', '2018-03-20 16:16:53'),
	(14, 'del_cate', 'cate', '2018-03-20 16:17:11', '2018-03-20 16:17:11'),
	(15, 'add_product', 'product', '2018-03-20 16:17:29', '2018-03-20 16:17:29'),
	(16, 'edit_product', 'product', '2018-03-20 16:17:50', '2018-03-20 16:17:50');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping data for table shophot.permission_role: ~4 rows (approximately)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
	(10, 1, '2018-03-20 16:14:50', '2018-03-20 16:14:50'),
	(10, 3, '2018-03-20 16:14:50', '2018-03-20 16:14:50'),
	(10, 5, '2018-03-20 16:14:50', '2018-03-20 16:14:50'),
	(10, 6, '2018-03-20 16:14:50', '2018-03-20 16:14:50');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping data for table shophot.products: ~11 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `alias`, `cate_id`, `address_id`, `start_day`, `end_day`, `price`, `dientich`, `name_own`, `phone`, `address_detail`, `images`, `mota`, `status`, `created_at`, `updated_at`, `thanhtien`, `pending`, `gmap`) VALUES
	(1, 'Có phòng trọ cho thuê xóm cây na', 'co-phong-tro-cho-thue-xom-cay-nahtml', 2, 1, '2018-03-24', '2018-03-31', 500, 15, 'Ông Mạnh', '09857563534', NULL, '1521859812.jpg', 'Phòng đầy đủ tiện nghi khép kín, đủ đồ bao gồm: -ĐIỀU HOÀ. -MÁY GIẶT. -GIƯỜNG NGỦ. -TỦ QUẦN ÁO. -KỆ BẾP. -WC KHÉP KÍN đầy đủ thiết bị vệ sinh, BÌNH NÓNG LẠNH. chỉ việc xách vali đến ở. \r\nDiện tích phòng 30- 35m2. Giá cho thuê: 3,5- 4tr/ tháng.\r\nPhòng có cửa sổ rất thoáng, ánh sáng ngập tràn. \r\nDịch vụ: -Điện: 3k/số. -Nước: 80k/người. -Net: 100k/phòng. Chỗ để xe máy FREE ở tầng 1. \r\nGiờ giấc ra vào TỰ DO THOẢI MÁI, ở không chung chủ. \r\nCó thể vào ở ngay. Có camera theo dõi và khoá cửa vân tay đảm bảo an ninh.', 1, '2018-03-24 02:50:12', '2018-03-24 02:50:12', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(2, 'Cho thuê phòng trọ xóm cây bông', 'cho-thue-phong-tro-xom-cay-bonghtml', 2, 2, '2018-03-24', '2018-04-04', 700, 20, 'Ông Tuấn', '0947745443', NULL, '1521859953.jpg', '- 2 phòng ở tầng 2 không khép kín , 2 phòng / tầng chung 1 wc khoảng 25m2/phòng giá 2tr7/phòng/tháng..uu tiên nhóm thuê cả tầng giá chỉ 5tr2/tháng\r\n- 1 phòng tầng 6 khoảng 18m2 không khép kín ,wc chung với 1 phòng bên cạnh giá 2tr2/tháng\r\n- 1 phòng tầng 4 khoảng 28m2 CÓ KHÉP KÍN giá 3trđ/tháng\r\n@@ gần các trường đại học lớn như Mỏ Địa Chất, hv Tài Chính, hv Cảnh Sát...', 1, '2018-03-24 02:52:33', '2018-03-24 02:52:33', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(3, 'Có phòng trọ cho thuê xóm cây tre', 'co-phong-tro-cho-thue-xom-cay-trehtml', 2, 4, '2018-03-24', '2018-03-31', 600, 15, 'Bà Trang', '0986756453', NULL, '1521860251.jpg', 'Nhà mới sạch sẽ đẹp như hình, 1 phòng ngủ, 1 vs ,1 ban công và có bếp khép kín đầy đủ. Diện tích 35m2\r\n- Đã đăng kí giấy phép kinh doanh cho người nước ngoài thuê ở, có thể mở văn phòng \r\n- Full nội thất đầy đủ: 1 giường 1m6x2m, tủ 3 cánh, tủ li, kệ tivi, bàn tủ bếp, bàn trang điểm, đệm, nóng lạnh, điều hòa.... rộng rãi thoáng mát chỉ việc chuyển đến ở luôn', 1, '2018-03-24 02:57:31', '2018-03-24 02:57:31', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(4, 'có phòng trọ cho thuê xóm cây chuối', 'co-phong-tro-cho-thue-xom-cay-chuoihtml', 2, 5, '2018-03-24', '2018-03-31', 600, 16, 'Ông Nam', '09874643647', NULL, '1521860400.jpg', '- 2 phòng ở tầng 2 không khép kín , 2 phòng / tầng chung 1 wc khoảng 25m2/phòng giá 2tr7/phòng/tháng..uu tiên nhóm thuê cả tầng giá chỉ 5tr2/tháng\r\n- 1 phòng tầng 6 khoảng 18m2 không khép kín ,wc chung với 1 phòng bên cạnh giá 2tr2/tháng\r\n- 1 phòng tầng 4 khoảng 28m2 CÓ KHÉP KÍN giá 3trđ/tháng\r\n@@ gần các trường đại học lớn như Mỏ Địa Chất, hv Tài Chính, hv Cảnh Sát...', 1, '2018-03-24 03:00:01', '2018-03-24 03:00:01', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(5, 'Có phòng trọ cho thuê Tùng Thảo Nguyên', 'co-phong-tro-cho-thue-tung-thao-nguyenhtml', 2, 6, '2018-03-24', '2018-03-31', 500, 14, 'Ông Lan', '097564654332', NULL, '1521860768.JPG', 'thiết bị cơ bản gồm : bình nóng lạnh , tủ lạnh , giường , tủ quần áo , bàn làm việc , máy giặt chung toàn căn hộ .\r\nđiện 3k/1 nước giá nhà nước . mạng 80k/1phong \r\ngiơ giấc tự quản đi đóng về mở khu dân trí cao an ninh tốt gần trường học bệnh viện và gần chợ', 1, '2018-03-24 03:06:08', '2018-03-24 03:06:08', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(6, 'Có phòng trọ cho thuê xóm cây trứng cá', 'co-phong-tro-cho-thue-xom-cay-trung-cahtml', 2, 3, '2018-03-24', '2018-04-30', 800, 20, 'Bà Mai', '09756452435', NULL, '1521860893.jpg', 'thiết bị cơ bản gồm : bình nóng lạnh , tủ lạnh , giường , tủ quần áo , bàn làm việc , máy giặt chung toàn căn hộ .\r\nđiện 3k/1 nước giá nhà nước . mạng 80k/1phong \r\ngiơ giấc tự quản đi đóng về mở khu dân trí cao an ninh tốt gần trường học bệnh viện và gần chợ', 1, '2018-03-24 03:08:13', '2018-03-24 03:08:13', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(7, 'Có phòng trọ cho thuê bà Thủy', 'co-phong-tro-cho-thue-ba-thuyhtml', 2, 8, '2018-03-24', '2018-04-30', 450, 15, 'Bà Thủy', '0987566454', NULL, '1521860983.jpg', 'thiết bị cơ bản gồm : bình nóng lạnh , tủ lạnh , giường , tủ quần áo , bàn làm việc , máy giặt chung toàn căn hộ .\r\nđiện 3k/1 nước giá nhà nước . mạng 80k/1phong \r\ngiơ giấc tự quản đi đóng về mở khu dân trí cao an ninh tốt gần trường học bệnh viện và gần chợ', 1, '2018-03-24 03:09:43', '2018-03-24 03:09:43', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(8, 'Có phòng trọ cho thuê xóm ông Huy', 'co-phong-tro-cho-thue-xom-ong-huyhtml', 2, 3, '2018-03-24', '2018-04-30', 400, 15, 'Ông Huy', '09876645342', NULL, '1521861088.jpg', 'thiết bị cơ bản gồm : bình nóng lạnh , tủ lạnh , giường , tủ quần áo , bàn làm việc , máy giặt chung toàn căn hộ .\r\nđiện 3k/1 nước giá nhà nước . mạng 80k/1phong \r\ngiơ giấc tự quản đi đóng về mở khu dân trí cao an ninh tốt gần trường học bệnh viện và gần chợ', 1, '2018-03-24 03:11:28', '2018-03-24 03:11:28', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(9, 'Bán nhà xóm cây bàng', 'ban-nha-xom-cay-banghtml', 1, 7, '2018-03-24', '2018-04-30', 400000, 120, 'Ông Thắng', '098657534', NULL, '1521861294.jpg', 'thiết bị cơ bản gồm : bình nóng lạnh , tủ lạnh , giường , tủ quần áo , bàn làm việc , máy giặt chung toàn căn hộ .\r\ngiơ giấc tự quản đi đóng về mở khu dân trí cao an ninh tốt gần trường học bệnh viện và gần chợ', 1, '2018-03-24 03:14:54', '2018-03-24 03:14:54', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3710.0226236249086!2d105.80419231456221!3d21.585039985699677!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352738b1bf08a3%3A0x515f4860ede9e108!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBDw7RuZyBuZ2jhu4cgVGjDtG5nIHRpbiAmIFRydXnhu4FuIHRow7RuZyBUaMOhaSBOZ3V5w6pu!5e0!3m2!1svi!2s!4v1522114656946'),
	(10, 'Có phòng trọ cho thuê gần cntt', 'co-phong-tro-cho-thue-gan-cntthtml', 2, 1, '2018-03-27', '2018-03-29', 500, 15, 'Ông Sinh', '09875632233', 'Gần Trà đá Sinh Mị, Z115, Quyết Thắng, Thành phố Thái Nguyên', '1522118998.jpg', '<p><span style="color: #313131; font-family: Arial;">Ph&ograve;ng x&acirc;y sạch đẹp, tho&aacute;ng m&aacute;t, WC trong ph&ograve;ng.&nbsp;</span><br style="color: #313131; font-family: Arial;" /><span style="color: #313131; font-family: Arial;">- Khu an ninh, gần chợ, si&ecirc;u thị, trường học, bệnh viện, gần trạm xe bu&yacute;t, ga S&agrave;i G&ograve;n, ....tuyến đường thuận tiện đi đến c&aacute;c quận kh&aacute;c trong th&agrave;nh phố.&nbsp;</span><br style="color: #313131; font-family: Arial;" /><span style="color: #313131; font-family: Arial;">- C&oacute; lối đi ri&ecirc;ng, chỗ để xe miễn ph&iacute;, truyền h&igrave;nh c&aacute;p, inte********** ổn định,&nbsp;</span></p>', 1, '2018-03-27 02:49:58', '2018-03-27 02:49:58', NULL, 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1855.0016536721464!2d105.80433365796938!3d21.58579399642483!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31352740aaf61979%3A0x7eac74d01bd8ad37!2zVHLDoCDEkcOhIFNpbmggTeG7iw!5e0!3m2!1svi!2s!4v1522118709194'),
	(12, 'Có phòng trọ xóm bà Ngân', 'co-phong-tro-xom-ba-nganhtml', 2, 3, '2018-03-27', '2018-03-30', 500, 16, 'Bà Ngân', '09857463535', 'Đê Nông Lâm', '1522133663.jpg', '<p><span style="font-family: Tahoma;">Căn hộ gồm đầy đủ trang thiết bị như: Giường, tủ, tivi Led, điều h&ograve;a, n&oacute;ng lạnh, b&agrave;n l&agrave;m việc, tủ quần &aacute;o, ban c&ocirc;ng rộng c&oacute; r&egrave;m cửa l&atilde;ng mạn.</span></p>', 1, '2018-03-27 06:54:23', '2018-03-27 07:49:10', '30.000', 1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14839.208529188203!2d105.80217622365583!3d21.593645371119823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDM1JzM3LjEiTiAxMDXCsDQ4JzM5LjQiRQ!5e0!3m2!1svi!2s!4v1522133001072');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping data for table shophot.roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table shophot.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
